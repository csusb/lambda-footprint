# encoding: utf-8

import os
import logging
import json
from datetime import datetime, timedelta
from ipaddress import ip_network, IPv4Network
from uuid import uuid4

import boto3
import nmap


if 'LAMBDA_TASK_ROOT' in os.environ:
    os.environ['PATH'] += os.pathsep + os.path.join(os.environ['LAMBDA_TASK_ROOT'], 'bin')


MIN_PREFIX_LEN = os.environ.get('MIN_PREFIX_LEN', 28)
NMAP_FLAGS = os.environ.get('NMAP_FLAGS', '-T4 --top-ports 100 -n -Pn -sT')
LOG_LEVEL = {
    'INFO': logging.INFO,
    'DEBUG': logging.DEBUG,
}.get(os.environ.get('LOG_LEVEL'), logging.INFO)


dynamodb = boto3.resource('dynamodb')
aws_lambda = boto3.client('lambda')


class HostReport(object):
    table_name = os.environ.get('TABLE_NAME', 'NMAPScanResults')
    ttl = int(os.environ.get('TABLE_TTL', '604800')) # 7 days (seconds)

    def __init__(self, host, scan_time=None, **data):
        self.host = host
        if scan_time is None:
            self.scan_time = datetime.now()
        else:
            self.scan_time = scan_time
        self.data = dict(**data)

    @property
    def expire_time(self):
        return self.scan_time + timedelta(seconds=self.ttl)

    @classmethod
    def from_nmap_host(cls, host, scan_time, nmap_host):
        data = {'State': nmap_host.state()}
        for protocol_name in nmap_host.all_protocols():
            for port_number in nmap_host[protocol_name].keys():
                port_status = {x: y for x, y in nmap_host[protocol_name][port_number].items() if y}
                protocol = data.setdefault(protocol_name, {})
                protocol[str(port_number)] = port_status
        return cls(host, scan_time, **data)

    @classmethod
    def host_down(cls, host, scan_time):
        return cls(host, scan_time, State='down')

    def as_dynamodb_item(self):
        item = {
            'Host': self.host,
            'ScanTime': self.scan_time.isoformat(),
            'ExpirationTime': self.expire_time.strftime('%s'),
        }
        item.update(self.data)
        return item

    def insert(self):
        table = dynamodb.Table(self.table_name)
        table.put_item(Item=self.as_dynamodb_item())


def _invoke(network, scan_time, context):
    logging.info('dispatching subscan for %s', network)
    payload = {
        'hosts': str(network),
        'ScanTime': scan_time.isoformat(),
    }
    aws_lambda.invoke(
        FunctionName=context.function_name,
        InvocationType='Event',
        Payload=json.dumps(payload),
    )


def scan(network, scan_time):
    assert isinstance(network, IPv4Network)

    scanner = nmap.PortScanner()

    logging.info('ScanTime: %s', scan_time.isoformat())
    logging.info('nmap %s %s', NMAP_FLAGS, network)

    all_hosts = scanner.listscan(hosts=str(network))
    scanner.scan(hosts=str(network), arguments=NMAP_FLAGS)
    logging.info('Scan Completed: %s', scanner.scanstats())

    for name in all_hosts:
        logging.debug(name)
        try:
            report = HostReport.from_nmap_host(name, scan_time, scanner[name])
        except KeyError:
            report = HostReport.host_down(name, scan_time)
        try:
            report.insert()
        except Exception:
            logging.exception('Failed to insert report')
            continue


def optimize_network_list(networks):
    optimized = []
    for network in networks:
        if network.prefixlen < MIN_PREFIX_LEN:
            for network in network.subnets(new_prefix=MIN_PREFIX_LEN):
                optimized.append(network)
        else:
            optimized.append(network)
    return optimized


def handler(event, context):
    logging.getLogger().setLevel(LOG_LEVEL)
    logging.info('Startup with log level: %s', LOG_LEVEL)

    try:
        scan_id = event['ScanID']
    except KeyError:
        scan_id = str(uuid4())

    try:
        scan_time = datetime.fromisoformat(event['ScanTime'])
    except KeyError:
        scan_time = datetime.now()

    table = dynamodb.Table(os.environ.get('SCAN_TABLE_NAME', 'NMAPScans'))
    table.put_item(Item={
        'ScanID': scan_id,
        'ScanTime': scan_time.isoformat()
    })

    if isinstance(event['Hosts'], str):
        hosts = optimize_network_list([ip_network(event['Hosts'])])
    elif isinstance(event['Hosts'], list):
        hosts = optimize_network_list([ip_network(x) for x in event['Hosts']])
    else:
        raise ValueError('Expected hosts as string or list of strings')

    if len(hosts) == 1:
        scan(hosts[0], scan_time)
    else:
        for network in hosts:
            _invoke(network, scan_time, context)

