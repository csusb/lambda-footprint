resource aws_dynamodb_table scans {
  name = "NMAPScans"

  billing_mode = "PAY_PER_REQUEST"

  hash_key  = "ScanID"
  range_key = "ScanTime"

  attribute {
    name = "ScanID"
    type = "S"
  }

  attribute {
    name = "ScanTime"
    type = "S"
  }
}

resource aws_dynamodb_table this {
  name = "NMAPScanResults"

  billing_mode = "PAY_PER_REQUEST"

  hash_key  = "Host"
  range_key = "ScanTime"

  ttl {
    attribute_name = "ExpirationTime"
    enabled        = true
  }

  attribute {
    name = "Host"
    type = "S"
  }

  attribute {
    name = "ScanTime"
    type = "S"
  }

  global_secondary_index {
    name     = "Scans"
    hash_key = "ScanTime"

    projection_type = "ALL"
  }
}
