# encoding: utf-8

import sys
import logging
import json
from pprint import pprint

import boto3


aws_lambda = boto3.client('lambda')


def test_invoke(function_name='footprint', hosts=None):
    if hosts is None:
        hosts = sys.argv[1:]

    payload = {
        'ScanID': 'test',
        'Hosts': hosts,
    }
    pprint(payload)
    response = aws_lambda.invoke(
        FunctionName=function_name,
        InvocationType='Event',
        Payload=json.dumps(payload),
    )
    pprint(response)


if __name__ == '__main__':
    test_invoke()
