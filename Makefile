default: function.zip

nmap:
	docker build -t nmap-builder static-toolbox/recipes/nmap/linux_x86_64
	docker run -v $$(pwd)/nmap:/output --rm nmap-builder
	sudo chown -R $$(whoami) nmap

function.zip: nmap function.py
	pip install --target ./build python-nmap
	mkdir -p build/bin
	mkdir -p build/share/nmap
	cp nmap/linux/x86_64/nmap-7.80SVN-ec30dba build/bin/nmap
	cp -r nmap/nmap-data-0.7.80SVN-ec30dba/nmap-* build/share/nmap
	cp function.py build/
	(cd build && zip -r9 -r - .) > function.zip

clean:
	rm -f function.zip
	rm -rf build

reallyclean:
	rm -rf nmap

deploy: function.zip
	(cd ~/Project/iset-terraform/its-dev-1 && terraform apply -auto-approve)

test:
	AWS_DEFAULT_PROFILE=its-dev-1 python tests/test_invoke.py 139.182.0.0/22
