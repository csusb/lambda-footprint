resource aws_cloudwatch_event_rule this {
  name                = "FootprintScheduledRun"
  schedule_expression = "rate(1 day)"
}

resource aws_cloudwatch_event_target this {
  rule = aws_cloudwatch_event_rule.this.name
  arn  = aws_lambda_function.this.arn

  input = <<EOF
{
  "ScanID": "main-campus",
  "Hosts": [
    "139.182.0.0/16"
  ]
}
EOF
}
