resource aws_lambda_permission this {
  action        = "lambda:InvokeFunction"
  principal     = "events.amazonaws.com"
  function_name = aws_lambda_function.this.function_name
  source_arn    = aws_cloudwatch_event_rule.this.arn
}

resource aws_lambda_function this {
  runtime       = "python3.7"
  filename      = "${path.module}/function.zip"
  function_name = "footprint"
  role          = aws_iam_role.this.arn
  handler       = "function.handler"
  timeout       = "900"

  source_code_hash = filebase64sha256("${path.module}/function.zip")

  environment {
    variables = {
      TABLE_NAME = aws_dynamodb_table.this.id
      SCAN_TABLE_NAME = aws_dynamodb_table.scans.id
    }
  }
}
